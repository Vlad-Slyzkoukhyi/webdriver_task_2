using AngleSharp.Dom;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Xml.Linq;

namespace WebDriver_Task_2
{
    [TestFixture]
    public class WebDriverTask2Test
    {
        private ChromeDriver _driver;
        private const string _chromeUrl = "https://pastebin.com/";        
        private const string _expirationValue = "10 Minutes";
        private const string _syntaxHighlightingValue = "Bash";
        private const string _inputText = "git config --global user.name  \"New Sheriff in Town\"\r\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\r\n" +
                "git push origin master --force\r\n";
        private const string _inputTitle = "how to gain dominance among developers";

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }

        [Test]
        public void CreateNewPaste()
        {
            WebDriverTask2Data pb = new(_driver);
            
            _driver.Navigate().GoToUrl(_chromeUrl);

            pb.Click(pb.NewPasteButton);
            pb.SendKeys(pb.PostformTextField, _inputText);
            ((IJavaScriptExecutor)_driver).ExecuteScript("window.scrollBy(0,500)", "");
            pb.Click(pb.DropDownSyntaxHighlightElement);
            pb.DropDownSyntaxHighlightOptions?.FirstOrDefault(x => x.Text.Equals(_syntaxHighlightingValue))?.Click();
            pb.Click(pb.DropDownElement);
            pb.DropDownOptions?.FirstOrDefault(x => x.Text.Equals(_expirationValue))?.Click();
            pb.SendKeys(pb.TitleField, _inputTitle);
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", pb.SubmitButton);
            pb.Click(pb.SubmitButton);
            Assert.That(pb.ExisteTitle?.Text, Is.EqualTo(_inputTitle), "The created title does not match the expected title.");
            Assert.That(pb.ExisteText?.Text.Trim(), Is.EqualTo(_inputText.Trim()), "The text content of the <div> element does not match the expected text.");
            Assert.That(pb.ExisteSyntax?.Text.Trim(), Is.EqualTo(_syntaxHighlightingValue.Trim()), "Syntax highlighting is not a Bash");
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Close();
            _driver.Quit();
        }
    }
}